package com.lts.service;

import com.lts.bean.User;

public interface UserService extends BaseService<User> {
    User get(User user);
}
