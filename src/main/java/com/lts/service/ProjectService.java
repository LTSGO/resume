package com.lts.service;

import com.lts.bean.Project;

public interface ProjectService extends BaseService<Project> {
}
