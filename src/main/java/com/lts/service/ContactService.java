package com.lts.service;


import com.lts.bean.Contact;
import com.lts.bean.ContactListParam;
import com.lts.bean.ContactListResult;

public interface ContactService extends BaseService<Contact> {
    ContactListResult list(ContactListParam param);
    boolean read(Integer id);
}
