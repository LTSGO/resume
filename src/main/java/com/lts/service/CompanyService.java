package com.lts.service;

import com.lts.bean.Company;

public interface CompanyService extends BaseService<Company> {
}
