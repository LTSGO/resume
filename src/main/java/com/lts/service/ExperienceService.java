package com.lts.service;

import com.lts.bean.Experience;

public interface ExperienceService extends BaseService<Experience> {
}
