package com.lts.service.impl;

import com.lts.bean.User;
import com.lts.dao.UserDao;
import com.lts.service.UserService;


public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

    @Override
    public User get(User user) {
        return ((UserDao) dao).get(user);
    }
}
