package com.lts.service.impl;


import com.lts.bean.Contact;
import com.lts.bean.ContactListParam;
import com.lts.bean.ContactListResult;
import com.lts.dao.ContactDao;
import com.lts.service.ContactService;

public class ContactServiceImpl extends BaseServiceImpl<Contact> implements ContactService {

    @Override
    public ContactListResult list(ContactListParam param) {
        return ((ContactDao) dao).list(param);
    }

    @Override
    public boolean read(Integer id) {
        return ((ContactDao) dao).read(id);
    }
}
