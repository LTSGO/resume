package com.lts.bean;

import com.lts.bean.base.BaseBean;

public class Website extends BaseBean {
    private String footer;

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }
}
