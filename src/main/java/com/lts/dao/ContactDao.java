package com.lts.dao;


import com.lts.bean.Contact;
import com.lts.bean.ContactListParam;
import com.lts.bean.ContactListResult;

public interface ContactDao extends BaseDao<Contact> {
    ContactListResult list(ContactListParam param);
    boolean read(Integer id);
}
