package com.lts.dao;

import com.lts.bean.User;

public interface UserDao extends BaseDao<User> {
    User get(User user);
}
