package com.lts.dao;

import com.lts.bean.Experience;

public interface ExperienceDao extends BaseDao<Experience> {

}
