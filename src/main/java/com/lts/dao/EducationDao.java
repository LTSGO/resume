package com.lts.dao;

import com.lts.bean.Education;

public interface EducationDao extends BaseDao<Education> {

}
