package com.lts.dao;

import com.lts.bean.Skill;

public interface SkillDao extends BaseDao<Skill> {
}
